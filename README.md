# Vue Observer Component



## Getting started

To make it easy for you to transition components when entering the screen and when leaving with fade-in & out transitions,
you can use this Vue 3 wrapper component and make your life much easier.
Everytime the Vobserver component enteres the viewscreen it fades in and erverytime it leaves it it fades out.
It is meant to be a plugin but at this moment only available as simple component.



## Installation

To install it simply copy it top your projects components folder by coning it and copying it.

`git clone `
`cp VObservable.vue [PROJECT/src/components]`

Then import it in your main ts and register it as an Vue 3 component.

`import VObservable from 'src/components/VObservable.vue';`
`app.component('V-Observer', VObservable);`

***
